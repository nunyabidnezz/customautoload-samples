module.exports = function(s,config,lang,app,io){
    var checkAllLoadedMonitors = function(){
        if(s.group){
            var groupKeys = Object.keys(s.group)
            groupKeys.forEach(function(key){
                var monitorIds = Object.keys(s.group[key].mon)
                monitorIds.forEach(function(id){
                    var monitorObject = s.group[key].mon[id]
                    var monitorConfig = s.group[key].mon_conf[id]
                    s.orphanedVideoCheck(monitorConfig,5,function(orphanedFilesCount){
                        console.log(key,id,orphanedFilesCount)
                    })
                })
            })
        }
    }
    checkAllLoadedMonitors()
    var everyHour = 1000 * 60 * 60
    setInterval(checkAllLoadedMonitors,everyHour)
}
