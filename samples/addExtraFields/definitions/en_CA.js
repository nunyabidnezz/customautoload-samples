module.exports = {
	"Monitor Settings": {
		"blocks": {
			"Identity": {
				"info": [{
						"name": "detail=cowId",
						"field": "Cow ID",
						"default": "1",
						"description": ""
					},
					{
						"name": "detail=addKittens",
						"field": "Add Kittens",
						"fieldType": "select",
						"default": "0",
						"example": "",
						"possible": [{
								"name": "No",
								"value": "0"
							},
							{
								"name": "Yes",
								"value": "1"
							}
						]
					}
				]
			}
		}
	},
	"Account Settings": {
		"blocks": {
			"Cat Finder": {
				"name": "Cat Finder",
				"color": "grey",
				"info": [{
					"name": "detail=findThatCat",
					"field": "Enabled",
					"description": "",
					"default": "0",
					"example": "",
					"fieldType": "select",
					"possible": [{
							"name": "No",
							"value": "0"
						},
						{
							"name": "Yes",
							"value": "1"
						}
					]
				}]
			}
		}
	}
}
