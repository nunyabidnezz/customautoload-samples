var fs = require('fs')
var async = require('async')
var request = require('request')
module.exports = function(s,config,lang,app,io){
    // Base Init >>
    if(config.plateRecognizerApiEndpoint === undefined)config.plateRecognizerApiEndpoint = 'https://api.platerecognizer.com/v1/plate-reader'
    if(!config.plateRecognizerLicenseKey){
        console.log('Shinobi - Failed to Load PlateRecognizer.com Module')
        console.log('!!! You do not have your "plateRecognizerLicenseKey" option set in your conf.json. !!!')
        console.log('!!! You must set this in order to use this module. !!!')
        console.log('!!! Get your FREE trial at : https://app.platerecognizer.com/accounts/signup/ !!!')
        return
    }
    s.ocvTx = function(data){
        onDetectorJpegData(s.group[data.ke].activeMonitors[data.id],data.frame)
    }
    s.getImageDimensions = function(details){
        var height
        var width
        if(
            details.detector_scale_y_object &&
            details.detector_scale_y_object !== '' &&
            details.detector_scale_x_object &&
            details.detector_scale_x_object !== ''
        ){
            height = details.detector_scale_y_object
            width = details.detector_scale_x_object
        }else{
            height = details.detector_scale_y
            width = details.detector_scale_x
        }
        return {
            height : parseFloat(height),
            width : parseFloat(width)
        }
    }
    // Base Init />>
    var queue = async.queue(function(task,callback){callback()},1)
    var detectLicensePlate = function(monitor,buffer){
        var img = s.getImageDimensions(s.group[monitor.ke].rawMonitorConfigurations[monitor.id].details)
        request({
          url: config.plateRecognizerApiEndpoint,
          method: 'POST',
          headers: {
            'Authorization': `Token ${config.plateRecognizerLicenseKey}`,
          },
          formData: {
              "upload": {
                value:  buffer,
                options: {
                  filename: `${s.gid()}.jpg`,
                  contentType: 'image/jpeg'
                }
              }
          }
        }, function(err,res,body){
            var response = s.parseJSON(body)
            var results = response.results
            if(response.detail){
                s.userLog(monitor,{type:'PlateRecognizer.com Module',msg:response.detail})
            }
            if(!results || results.length === 0){
                return
            }
            var highestResult = {score: 0}
            results.forEach(function(result){
                if(highestResult.score < result.score){
                    highestResult = result
                }
            })
            var matrix = {
                x : highestResult.box.xmin,
                y : highestResult.box.ymin,
                width : highestResult.box.xmax - highestResult.box.xmin,
                height : highestResult.box.ymax - highestResult.box.ymin,
                tag : highestResult.plate.toUpperCase(),
                confidence : highestResult.score,
            }
            s.triggerEvent({
                f: 'trigger',
                id: monitor.id,
                ke: monitor.ke,
                details:{
                    plug: 'PlateRecognizerModule',
                    name: 'licensePlate',
                    reason: 'object',
                    matrices: [matrix],
                    imgHeight: img.height,
                    imgWidth: img.width,
                    procTime: response.processing_time
                }
            })
        })
    }
    var onDetectorJpegData = function(d,buffer){
        queue.push({
            name: d.ke + d.id
        },function(err){
            if(!s.group[d.ke].activeMonitors[d.id].buffer){
                s.group[d.ke].activeMonitors[d.id].buffer = [buffer]
            }else{
                s.group[d.ke].activeMonitors[d.id].buffer.push(buffer)
            }
            if(buffer[buffer.length-2] === 0xFF && buffer[buffer.length-1] === 0xD9){
                var completeBuffer = Buffer.concat(s.group[d.ke].activeMonitors[d.id].buffer)
                s.group[d.ke].activeMonitors[d.id].buffer = null
                detectLicensePlate(d,completeBuffer)
            }
        })
    }
    s.pluginInitiatorSuccess('host',{
        id: 'PlateRecognizerModule',
        plug: 'PlateRecognizer',
        type: 'detector',
        notice: `Get your FREE trial for Plate Recognizer at : <a href="https://app.platerecognizer.com/accounts/signup/">platerecognizer.com</a>`,
        connectionType: 'module'
    })
    // s.onMonitorDetectorDataOutputAlone = onDetectorJpegData
    // s.onMonitorDetectorDataOutputSecondary = onDetectorJpegData
    console.log('Shinobi - Loaded PlateRecognizer.com Module')
}
