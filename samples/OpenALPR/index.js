var fs = require('fs')
var async = require('async')
var request = require('request')
var openalpr = {
    us: require ("node-openalpr-shinobi"),
    eu: require ("node-openalpr-shinobi"),
}
module.exports = function(s,config,lang,app,io){
    // Base Init >>
    if(config.alprConfig === undefined){
        config.alprConfig = s.mainDirectory + '/plugins/openalpr/openalpr.conf'
    }
    Object.keys(openalpr).forEach(function(region){
        openalpr[region].Start(config.alprConfig, null, null, true, region)
    })
    var convertResultsToMatrices = function(results){
        var mats = []
        var plates = []
        results.forEach(function(v){
            v.candidates.forEach(function(g,n){
                if(v.candidates[n].matches_template){
                    delete(v.candidates[n].matches_template)
                }
            })
            plates.push({
                coordinates: v.coordinates,
                candidates: v.candidates,
                confidence: v.confidence,
                plate: v.plate
            })
            var width = Math.sqrt( Math.pow(v.coordinates[1].x - v.coordinates[0].x, 2) + Math.pow(v.coordinates[1].y - v.coordinates[0].y, 2));
            var height = Math.sqrt( Math.pow(v.coordinates[2].x - v.coordinates[1].x, 2) + Math.pow(v.coordinates[2].y - v.coordinates[1].y, 2))
            mats.push({
                x: v.coordinates[0].x,
                y: v.coordinates[0].y,
                width: width,
                height: height,
                tag: v.plate
            })
        })
        return mats
    }
    var getImageDimensions = function(details){
        var height
        var width
        if(
            details.detector_scale_y_object &&
            details.detector_scale_y_object !== '' &&
            details.detector_scale_x_object &&
            details.detector_scale_x_object !== ''
        ){
            height = details.detector_scale_y_object
            width = details.detector_scale_x_object
        }else{
            height = details.detector_scale_y
            width = details.detector_scale_x
        }
        return {
            height : parseFloat(height),
            width : parseFloat(width)
        }
    }
    var queue = async.queue(function(task,callback){callback()},1)
    var detectLicensePlate = function(monitor,buffer){
        try{
            var monitorConfig = s.group[monitor.ke].rawMonitorConfigurations[monitor.id].details
            var img = getImageDimensions(monitorConfig)
            var region = monitorConfig.detector_lisence_plate_country || 'us'
            openalpr[region].IdentifyLicense(buffer, {}, function (error, output){
                var results = output.results
                if(results.length > 0){
                    var matrices = convertResultsToMatrices(results)
                    s.triggerEvent({
                        f: 'trigger',
                        id: monitor.id,
                        ke: monitor.ke,
                        details:{
                            plug: 'OpenALPRModule',
                            name: 'licensePlate',
                            reason: 'object',
                            matrices: matrices,
                            imgHeight: img.height,
                            imgWidth: img.width
                        }
                    })
                }
            })
        }catch(err){
            console.log(err)
        }
    }
    var onDetectorJpegData = function(d,buffer){
        // queue.push({
        //     name: d.ke + d.id
        // },function(err){
            if(!s.group[d.ke])console.log(new Error(),d)
            if(!s.group[d.ke].activeMonitors[d.id].OpenALPRImageBuffer){
                s.group[d.ke].activeMonitors[d.id].OpenALPRImageBuffer = [buffer]
            }else{
                s.group[d.ke].activeMonitors[d.id].OpenALPRImageBuffer.push(buffer)
            }
            if(buffer[buffer.length-2] === 0xFF && buffer[buffer.length-1] === 0xD9){
                var completeBuffer = Buffer.concat(s.group[d.ke].activeMonitors[d.id].OpenALPRImageBuffer)
                s.group[d.ke].activeMonitors[d.id].OpenALPRImageBuffer = null
                detectLicensePlate(d,completeBuffer)
            }
        // })
    }
    // Base Init />>
    s.ocvTx = function(data){
        if(data.f === 'readPlugins' || data.f === 'init_monitor'){return false}
        onDetectorJpegData(s.group[data.ke].rawMonitorConfigurations[data.id],data.frame)
    }
    s.pluginInitiatorSuccess('host',{
        id: 'OpenALPRModule',
        plug: 'OpenALPR',
        type: 'detector',
        notice: `asdasd`,
        connectionType: 'module'
    })
    console.log('Shinobi - Loaded OpenALPR Module')
}
