SCRIPT=$(readlink -f "$0")
# Absolute path this script is in, thus /home/user/bin
SCRIPTPATH=$(dirname "$SCRIPT")
echo $SCRIPTPATH

DIR=`dirname $0`
INSTALLERS_DIR="$DIR/../../../INSTALL"
if ! [ -x "$(command -v dos2unix)" ]; then
    echo "-----------------------------------"
    echo "Installing dos2unix"
    apt install dos2unix -y
fi
echo "-----------------------------------"
if ! [ -x "$(command -v alpr)" ]; then
    echo "Installing OpenALPR"
    echo "Do you want to Install OpenALPR with CUDA enabled?"
    echo "(Y)es or (n)o?"
    echo "Press [ENTER] for default (Yes)"
    read openalprcudaenabled
    if [ "$openalprcudaenabled" = "n" ] || [ "$openalprcudaenabled" = "N" ]; then
        sed -i -e 's/detector = lbpgpu/detector = lbpcpu/g' "$DIR/openalpr.conf"
        dos2unix $INSTALLERS_DIR/openalpr-cpu-easy.sh
        sh $INSTALLERS_DIR/openalpr-cpu-easy.sh
    else
        sed -i -e 's/detector = lbpcpu/detector = lbpgpu/g' "$DIR/openalpr.conf"
        dos2unix $INSTALLERS_DIR/openalpr-gpu-easy.sh
        sh $INSTALLERS_DIR/openalpr-gpu-easy.sh
    fi
else
    echo "OpenALPR found... : $(alpr --version)"
fi
echo "-----------------------------------"
echo "Installing Modules.."
cd $DIR/../../../
apt install node-pre-gyp -y
npm install nopt npmlog rimraf semver -g
npm install node-openalpr-shinobi --unsafe-perm --prefix "$SCRIPTPATH"
npm uninstall nopt npmlog rimraf semver -g
echo "Finding and Fixing Module Vulnerabilities.."
npm audit fix --force
