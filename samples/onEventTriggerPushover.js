// Script to send push notifications via the Pushover service (pushover.net)
//
// Configuration to add in conf.json: 
//  "pushover": {
//      "token": "YOUR_PUSHOVER_API_KEY",
//      "user": "YOUR_PUSHOVER_USER_KEY",
//      "monitors": ["MONITOR_ID", ...],
//      "timeout": 15,
//      "longTimeout": 60
//  }
//
//  Required settings:
//   token: Your API key/token
//   user:  Your user key
//  Optional settings:
//   monitors:    List of monitor IDs for which you want notifications. If omitted, notify for everything.
//   timeout:     In the event of rapid re-triggers, you probably don't want a million notifications. Motion
//                (triggering) has to stop for at least this many seconds before you get another notification.
//                Default: 15 seconds
//   longTimeout: If motion (triggering) repeats for at least this many seconds, then send a "continued motion"
//                notification (once). Set to a negative number to disable this "continued motion" notification
//                Default: 60 seconds

const fetch = require('node-fetch')
const fs = require('fs')
const FormData = require('form-data')
const exec = require('child_process').exec

module.exports = function(s,config,lang,app,io){
    var onTriggerEventForPushover = function(d,filter){
        if (('monitors' in config.pushover) && !config.pushover.monitors.includes(d.id)) {
            return
        }
        // Alert logic:
        // 1. when we start triggering, fire off a notification
        // 2. ignore continued triggers that come rapidly, since it's the same motion event continuing
        // 3. if triggering continues for a long time, fire one more notification to warn that we are
        //    suppressing notifications for extended triggers (e.g. someone is gardening in front of
        //    the camera... or buggy Dahua firmware is stuck firing FTP notifications until it sees a
        //    new real motion event, ugh)
        // 4. ignore continued rapid triggers
        let now = Date.now()
        if (!('alertTimeout' in s.group[d.ke].activeMonitors[d.id]))
            s.group[d.ke].activeMonitors[d.id].alertTimeout = {'start': 0, 'last': 0, 'sustained': false}
        last = s.group[d.ke].activeMonitors[d.id].alertTimeout.last
        s.group[d.ke].activeMonitors[d.id].alertTimeout.last = now
        if (now - last > config.pushover.timeout) {
            // 1. We are starting a new period of motion
            s.group[d.ke].activeMonitors[d.id].alertTimeout.start = now
            s.group[d.ke].activeMonitors[d.id].alertTimeout.sustained = false
        } else {
            // We're seeing continued rapid triggers...
            if (s.group[d.ke].activeMonitors[d.id].alertTimeout.sustained) {
                // 4. Motion is sustained and we already reported it, so ignore
                return
            } else {
                if (config.pushover.longTimeout > 0 && now - s.group[d.ke].activeMonitors[d.id].alertTimeout.start > config.pushover.longTimeout) {
                    // 3. Motion has now become sustained, so report it
                    s.group[d.ke].activeMonitors[d.id].alertTimeout.sustained = true
                } else {
                    // 2. Motion isn't long enough to be "sustained" yet, but has already been reported, so ignore it
                    return
                }
            }
        }
        s.mergeDetectorBufferChunks(d,function(mergedFilepath,filename){
            let gifFilepath = mergedFilepath.replace('.mp4', '.gif')
            // Pushover limtis attachments to 2.5MB
	    // 5 second skip is a hack to compensate for latency in the stream and may be system-specific
            // convert to a GIF, skip 5 seconds,                    5 fps, 2x speed,   max dimension 400px,                                                 magic to get a 256-color GIF palette
            exec('ffmpeg -ss 5 -i ' + mergedFilepath + ' -filter:v "fps=5,setpts=PTS/2,scale=w=400:h=400:force_original_aspect_ratio=decrease:flags=lanczos,split[s0][s1];[s0]palettegen=stats_mode=diff[p];[s1][p]paletteuse=diff_mode=rectangle" ' + gifFilepath, function(error, stdout, stderr) {
                if (error) {
                    console.log(`exec error converting to GIF for pushover: ${error}`)
                    return
                }
                var stats = fs.statSync(gifFilepath)
                const url='https://api.pushover.net/1/messages.json'
                const formData = new FormData()
                formData.append('token', config.pushover.token)
                formData.append('user', config.pushover.user)
                let message=s.group[d.ke].rawMonitorConfigurations[d.id].name +
                    ((s.group[d.ke].activeMonitors[d.id].alertTimeout.sustained) ? ' sustained motion; suppressing further notifications' : ' triggered');
                formData.append('message', message)
                formData.append('attachment', fs.readFileSync(gifFilepath), gifFilepath.replace(/.*\//, ''))
                fetch(url, {
                    method: 'POST',
                    body: formData
                }).then(function(res) {
                    fs.unlink(gifFilepath, function() {})
                    return res.json()
                }).then(function(jsonData){ 
                    if ('status' in jsonData && jsonData.status == 1) {
                        //console.log('push notification sent successfully!')
                    } else {
                        throw 'Unexpected response from pushover: ' + JSON.stringify(jsonData)
                    }
                }).catch(function(err) {
                    console.log('Error sending push notification')
                    console.log(err)
                })
            })
        })
    }
    if ('pushover' in config) {
        if (!('token' in config.pushover)) {console.log('Bad config for pushover, missing token'); return}
        if (!('user' in config.pushover)) {console.log('Bad config for pushover, missing user'); return}
        config.pushover.timeout = (config.pushover.timeout || 15)*1000
        config.pushover.longTimeout = (config.pushover.longTimeout || 60)*1000
        if (config.pushover.longTimeout > 0 && config.pushover.longTimeout <= config.pushover.timeout) {console.log(`Bad config for pushover, longTimeout ${config.pushover.longTimeout} < timeout ${config.pushover.timeout}`); return}
        s.onEventTrigger(onTriggerEventForPushover)
        console.log('Pushover notifications configured successfully')
    } else {
        console.log('Pushover notifications not configured')
    }
}
