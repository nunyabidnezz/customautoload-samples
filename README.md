# customAutoLoad Library Samples for Shinobi

### To use the samples

> By default the install location of Shinobi is `/home/Shinobi`.

1. Copy the **.js** file or **folder** into the `libs/customAutoLoad` folder found in your Shinobi directory.
2. Restart Shinobi.
    ```
    pm2 flush && pm2 restart camera && pm2 logs
    ```


# Modules made by Community Members
> These modules or repository links are not released under Shinobi Systems. You must acquire permission from the owner of the module to use in circumstances outside their set licensing.

|Name|Author  |Description |Link|
|--|--|--|--|
|MQTT Module|@ovivtash  | Connecting Shinobi CCTV and your MQTT broker of choice together! |https://gitlab.com/ovivtash/shinobi-mqtt
|MQTT Module|@geerd  | Connecting Shinobi CCTV and your MQTT broker of choice together! |https://gitlab.com/geerd/shinobi-mqtt
